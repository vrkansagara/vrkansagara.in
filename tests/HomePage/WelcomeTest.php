<?php

namespace Tests\HomePage;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    /** @test */
    public function HomePageIsWorking()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
