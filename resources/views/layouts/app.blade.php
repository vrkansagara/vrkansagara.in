<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Vallabh Kansagara - @vrkansagara - blog">
    <meta name="author" content="Vallabh Kansagara - @vrkansagara">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'VRKANSAGARA') }}</title>

    <link rel="canonical" href="{{route('/')}}">

    <!-- Custom styles for this template -->
    <link href="{{ mix('/assets/css/app.css') }}" rel="stylesheet">

  </head>
  <body class="d-flex flex-column h-100">
    
<header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('/')}}">{{config('app.name')}}</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav me-auto mb-2 mb-md-0">
		  <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown" data-bs-toggle="dropdown" aria-expanded="false">Projects</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown">
              <li><a class="dropdown-item" href="#">Action</a></li>
              <li><a class="dropdown-item" href="#">Another action</a></li>
              <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
          </li>
            @if (Route::has('login'))
                    @auth
						<li  class="nav-item"> <a class="nav-link" href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                    @else
						<li  class="nav-item"> <a class="nav-link" href="{{route('login')}}">Login</a></li>
                        @if (Route::has('register'))
							<li  class="nav-item"> <a class="nav-link" href="{{route('register')}}">Register</a></li>
                        @endif
                    @endauth
                </div>
            @endif
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>
</header>

<!-- Begin page content -->
<main class="flex-shrink-0">
            @yield('content')
</main>

<footer class="footer mt-auto py-3 bg-light">
<div class="container">
<span class="text-muted"> 
<a class="mr-5" href="#">Laravel v{{ Illuminate\Foundation\Application::VERSION }}</a> 
<a class="mr-5" href="#">PHP v{{ PHP_VERSION }}</a>
<span class="mr-5" >Slow and steady wins the race ....Vallabh Kansagara (@vrkansagara) </span>
</span>
</div>
</footer>


<script src="{{ mix('/assets/js/manifest.js') }}"></script>
<script src="{{ mix('/assets/js/vendor.js') }}"></script>
<script src="{{ mix('/assets/js/app.js') }}"></script>

      
  </body>
</html>
