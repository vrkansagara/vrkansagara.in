@extends('layouts.app')

@section('content')

<div class="container">
    <div class="badge-base LI-profile-badge" data-locale="en_US" data-size="large" data-theme="light" data-type="HORIZONTAL" data-vanity="vrkansagara" data-version="v1"><a class="badge-base__link LI-simple-link" href="https://in.linkedin.com/in/vrkansagara?trk=profile-badge">Vallabhdas Kansagara</a></div>
    <h1 class="mt-5">{{ config("app.name")}}</h1>
</div>

@endsection
