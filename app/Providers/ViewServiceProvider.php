<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use function app;
use function config;
use function sprintf;
use function view;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        $currentEnvironment = app()->environment();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        view()->share('emailLogo', sprintf("%s%s", config("app.url"), "/assets/images/logo.png");
        view()->share('appName', config('app.name'));
    }
}
