<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

use function app;
use function error_reporting;
use function in_array;
use function ini_set;

use const E_ALL;
use const E_DEPRECATED;
use const E_STRICT;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Avoid issue :- 1071 Specified key was too long; max key length is 767 bytes @START
         */
        Schema::defaultStringLength(191);
        /**
         * Avoid issue :- 1071 Specified key was too long; max key length is 767 bytes @END
         */

        /**
         * Log every SQL queries @START
         */
        $currentEnvironment = app()->environment();
        if (! in_array($currentEnvironment, ['local'])) {
            // Telescope css was loading with http which cause issue
            URL::forceScheme('https');
        }
        /**
         * Log every SQL queries @END
         */

        /**
         * Error reporting only available except Production environment @START
         */
        if ($this->app->isProduction() || app()->environment() === 'prod') {
            error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
            ini_set('display_errors', '0');
            ini_set("display_startup_errors", '0');
            ini_set("log_errors", '1');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
            ini_set("display_startup_errors", '1');
            ini_set("log_errors", '1');
        }
        /**
         * Error reporting only available except Production environment @END
         */

        DB::listen(
            function (QueryExecuted $query) {
                $data = [
                'query'    => $query->sql,
                    'bindings' => $query->bindings,
                    'time'     => $query->time,
                    ];
                    $time = $query->time;

                if ($time <= 100) {
                    Log::info('::SQL query::Normal', $data);
                } elseif ($time > 100 && $time <= 250) {
                    Log::warning('::SQL query:: Need to check', $data);
                } elseif ($time > 250 && $time <= 400) {
                    Log::alert('::SQL query:: Optimize as early', $data);
                } else {
                    Log::critical('::SQL query:: Need to FIX ON PRIORITY', $data);
                }
            }
        );
    }
}
